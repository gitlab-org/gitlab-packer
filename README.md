# DEPRECATED due to [unreliable Packer builds](https://gitlab.com/gitlab-org/gitlab-ce/issues/164#note_75698)

## I've downloaded a Virtual Machine Image from Gitlab.com!
If you have downloaded the .zip from [Gitlab.com/downloads](https://www.gitlab.com/downloads/) you can simply unzip the file and use the OVF file (and .vmdk) on you system of choice. Below you can find a small guide to set up Gitlab on VirtualBox.

### Run Gitlab from VirtualBox
You can run Gitlab from [VirtualBox](https://www.virtualbox.org/wiki/Downloads) quite easily:
With VirtualBox, open the gitlab-ubuntu.ovf to fire up the Gitlab server.
You have to make sure that in the VirtualBox settings the network is set as **Bridged Adapter**. You can find this under `Devices > Network Settings..`. If this value is wrong, restart the virtual machine after changing the network settings.

Login to the machine by using username `root` and password `g1tl5bftw`. It is advisable to change this if you are going to use this server for any amount of time.
To access your server from outside, all you need is the ipaddress of you VM. You can find this easily with:

```
ifconfig | grep addr
```

The first ip address that you see, is the address you can use to access your Gitlab instance. You can now log in with username `admin@local.host` and password `5iveL!fe`. If the IP address is 10.0.2.15, that means VirtualBox network settings are not correct _or_ you need to restart the machine.

# Packer.io script development for Gitlab
[Packer](http://packer.io) is a nice tool that creates a virtual machine images.
With this script you can create a Gitlab image for the host of your choice. After creating the image, it should only take a few seconds to launch your Gitlab instance.

With this script you can create the following images. Feel free to make to merge request for more.

- Gitlab 6.5 on Ubuntu 12.04 for DigitalOcean: do-ubuntu
- Gitlab 6.5 on CentOS 6.4 for DigitalOcean: do-centos
- Gitlab 6.5 on Ubuntu 12.04 as OVF / OVA: ovf-ubuntu
- Gitlab 6.5 on CentOS 6.5 as OVF / OVA: ovf-centos

# Usage
Install packer by following the [official guide](http://www.packer.io/docs/installation.html).

Next, edit **solo.json** to set your Gitlab preferences.

Depending on which builders you'll want to use, add the required variables for your builders in **builder_variables.json**. You can ignore the variables for builders you are not using.
You can also choose to provide these variables inline. Inline defined variables overwrite any predefined variables.

You can now run the `packer build gitlab.json` command for the image(s) you are interested in. If you want to only build a single or specific images, use the `-only buildname1, buildname2` flag. You will also need to specify the variables file or the separate variables.

```
packer build -only do-ubuntu-gitlab \
             -var-file=builder_variables.json \
             gitlab.json
```

After running the command above, packer will start building GitLab images for all specified platforms. This will give you ample time to drink some coffee while exploring the exciting open source repositories at gitlab.com. 

# Builders


## DigitalOcean
DigitalOcean supplies a Gitlab image for you to use, but you are free to create one yourself using this script. 

You are required to supply the DigitalOcean API key and client id to be able to use this builder.

```
packer build -only do-ubuntu, do-centos \
             -var 'digitalocean_client_id=yourclientid' \
             -var 'digitalocean_api_key=yourapikey' \
             gitlab.json
```

By default Gitlab will be installed on a 4gb (id 64) droplet in Amsterdam (id 2). You can overwrite these values by appending the appropriate values in your build command. *It is however not recommended to try to create the image in a smaller instance*, as you will run into a lack of resources. After creating the image, you are free to deploy it to any size droplet that you'd like. We recommend not using a droplet smaller than 1gb.
Also note that you can move the location of an image after creating it.

```
packer build -only do-ubuntu, do-centos \
             -var 'digitalocean_client_id=yourclientid' \
             -var 'digitalocean_api_key=yourapikey' \
             -var 'digitalocean_size_id=61' \
             -var 'digitalocean_region_id=1' \
             gitlab.json
```

**After creating the image, the used droplet is destroyed. However, you will be billed for the time that the droplet was active, while installing Gitlab.**


## VirtualBox OVF / OVA
To create a virtual machine in the Open Virtualization Format (OVF) or Open Virtual Appliance format (OVA), Packer can use [VirtualBox](https://www.virtualbox.org/). 

For this builder, VirtualBox is required and can be [downloaded for free](https://www.virtualbox.org/wiki/Downloads). 

```
packer build -only ovf-ubuntu \
             gitlab.json
```

Packer will download the Ubuntu or CentOS image automatically and configure it according to the preseed.cfg using VirtualBox. You are free to modify or use your own preseed.cfg, just make sure you have an ssh user available that matches with the user in the gitlab.json.

The CentOS build needs considerable resources to run for a reasonable time and not time out. Consider increasing the amount of cpus and memory allocated in gitlab.json if you are struggling with timeouts.

**The VM will be preconfigured with a rootpassword of `g1tl5bftw`. It is recommended that you change this. You also have the option to change this in the http/preseed.cfg and in the gitlab.json file before starting the build.**

### Variables

#### ovf_format
By default the builder will use OVF, but you can choose to generate an OVA package instead.

```
packer build -only ovf-ubuntu \
             -var 'ovf_format=ova' \
             gitlab.json
```

#### ovf_boot_wait
By default the builder will wait 10s for the virtual machine to have booted. If your machine is under heavy load, this might require more time.

```
packer build -only ovf-ubuntu \
             -var 'ovf_boot_wait=2m' \
             gitlab.json
```

Note: You are required to enable virtualization on your system (VT-x for Intel; AMD-V for AMD processors). Depending on your BIOS manufacturer, this is usually called Virtualization in your BIOS setup: enable this if it isn't already.

## TODO

- AWS
- Docker
- Openstack
- VMWare
- Google Compute Engine
- QEMU

# Post-Processors

## Vagrant

TODO: Create a vagrant box from one or more of the builders.
